# Dockerfile
FROM python:3.7-alpine3.10

USER root

ENV APP /app/pyblog

RUN mkdir $APP
WORKDIR $APP

EXPOSE 8770

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .

ENTRYPOINT [ "gunicorn" ]
CMD ["--workers", "2", "--bind", "0.0.0.0:8680", "blog:app"]

