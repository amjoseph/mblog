flask==1.1.4
flask_cors==3.0.8
flask[async]==1.0.1
pyjwt==1.7.1
Flask-Injector==0.12.3
injector==0.18.4
requests==2.22.0
gunicorn==20.1.0