import asyncio
import threading
from functools import wraps

from flask import Flask, jsonify, request, make_response
from werkzeug.middleware.dispatcher import DispatcherMiddleware

print(f"In flask global level: {threading.current_thread().name}")
app = Flask(__name__)

APPLICATION_ROOT = '/blog'
app.config['APPLICATION_ROOT'] = APPLICATION_ROOT
blog_content = {}

loop = asyncio.get_event_loop()

def permission_required(function):
    @wraps(function)
    def wrapper(*args, **kwargs):
        print("wrapper running!")
        return function(*args, **kwargs)
    return wrapper


@app.route("/<int:blog_id>", methods=["GET"])
@permission_required
def get_blog(blog_id):
    """

    :param blog_id:
    :return:
    """
    result = loop.run_until_complete(get_blog_by_id(blog_id))
    response_obj = {"status": "SUCCESS", "message": "Blog retrieved successfully", "data": result}
    return jsonify({"result": response_obj})


@app.route("/", methods=["POST"])
@permission_required
def create_blog():
    """
     {"id":23, "author":"", "title":"", "content":"", "date":""}
    :return:
    """
    try:
        request_data = request.get_json()
        # validate schema
    except Exception as e:
        response_object = {
            'status': 'FAILED',
            'message': e.message
        }
        return make_response(jsonify(response_object)), 400

    loop.run_until_complete(add_blog(request_data))
    response_obj = {"status": "SUCCESS", "message": "Blog created successfully"}
    return jsonify({"result": response_obj})


async def add_blog(details):
    key = details.get('id')
    global blog_content
    blog_content.update({key: details})
    return key


async def get_blog_by_id(blog_id):
    return blog_content.get(blog_id, {})


@app.before_request
def before_request():
    # validate
    return


def default_not_supported(env, resp):
    resp(b'200 OK', [(b'Content-Type', b'text/plain')])
    return [b'Endpoint not supported ;-)']


app.wsgi_app = DispatcherMiddleware(default_not_supported, {APPLICATION_ROOT: app.wsgi_app})
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8770, debug=False)
